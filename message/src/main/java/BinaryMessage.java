import net.openhft.chronicle.bytes.Bytes;
import net.openhft.chronicle.bytes.BytesIn;
import net.openhft.chronicle.bytes.BytesOut;
import net.openhft.chronicle.core.io.IORuntimeException;
import net.openhft.chronicle.queue.ChronicleQueue;
import net.openhft.chronicle.queue.ExcerptAppender;
import net.openhft.chronicle.queue.ExcerptTailer;
import net.openhft.chronicle.queue.impl.single.SingleChronicleQueueBuilder;
import net.openhft.chronicle.wire.AbstractBytesMarshallable;

import java.nio.ByteBuffer;

import static net.openhft.chronicle.bytes.Bytes.wrapForRead;

public class BinaryMessage extends AbstractBytesMarshallable {
    private int messageIndex = 0;

    private ByteBuffer byteBuffer;
    private Bytes<?> bytes;

    public BinaryMessage(ByteBuffer byteBuffer){
        this.byteBuffer = byteBuffer;
        this.bytes = Bytes.wrapForRead(this.byteBuffer);
    }

    public void readMarshallable(BytesIn bytesIn) throws IORuntimeException {
        this.field1.write((char) bytesIn.readByte());
        this.field3.write((char) bytesIn.readByte());
        this.field2.write((char) bytesIn.readByte());
    }

    public void writeMarshallable(BytesOut bytesOut) {
        bytesOut.writeByte((byte) this.field1.read());
        bytesOut.writeByte((byte) this.field3.read());
        bytesOut.writeByte((byte)this.field2.read());
    }

    public void setField1(char value){
        this.field1.write(value);
    }

    public void setField2(char value){
        this.field2.write(value);
    }

    public void setField3(char value){
        this.field3.write(value);
    }

    class Field {
        private int fieldIndex;
        private int fieldSize;

        public Field(int fieldIndex, int fieldSize){
            this.fieldIndex = fieldIndex;
            this.fieldSize = fieldSize;
            messageIndex += this.fieldSize;
        }

        public int getFieldIndex() {
            return fieldIndex;
        }
    }

    class Char extends Field{

        public Char() {
            super(messageIndex, 1);
        }

        public void write( char value){
            bytes.writeByte(getFieldIndex(), value);
        }

        public char read(){
            return (char) bytes.readByte(getFieldIndex());
        }
    }

    Char field1 = new Char();
    Char field2 = new Char();
    Char field3 = new Char();


    public static void main (String[] args){
        ByteBuffer writeBuffer = ByteBuffer.allocateDirect(256);
        ByteBuffer readBuffer = ByteBuffer.allocateDirect(256);

        BinaryMessage binaryMessageWrite = new BinaryMessage(writeBuffer);

        binaryMessageWrite.setField2( 'N');
        binaryMessageWrite.setField1( 'G');
        binaryMessageWrite.setField3( 'S');

        Bytes bytesWrite = Bytes.wrapForRead(binaryMessageWrite.byteBuffer);
        System.out.println(bytesWrite.toHexString());

        ChronicleQueue queue = SingleChronicleQueueBuilder.binary("/trades").build();

        ExcerptAppender appender = queue.acquireAppender();
        ExcerptTailer tailer =  queue.createTailer();

        appender.writeBytes(binaryMessageWrite);

        BinaryMessage binaryMessageRead = new BinaryMessage(readBuffer);

        tailer.readBytes(binaryMessageRead);

        Bytes bytes = Bytes.wrapForRead(binaryMessageRead.byteBuffer);

        System.out.println(bytes.toHexString());
    }
}
