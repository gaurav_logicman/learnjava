package com.gaurav.timer;

import java.util.ArrayList;

public class TimerManager extends Thread {

    Clock clock = new ClockImpl();
    ArrayList<Timer> timers = new ArrayList<>();

    public TimerManager() {

    }

    @Override
    public void run() {
        //Do soeme other work
        long epochTime = clock.getEpocNano();
        while (runNextTimer(epochTime) >= 0) {

        }
    }


    private long runNextTimer(long epocNano) {
        TimerImpl timer = (TimerImpl) getNext(epocNano);
        if (timer != null) {
            timer.timerTask.onTimer(timer);
            return clock.getEpocNano();
        } else {
            return -1;
        }
    }

    private Timer getNext(long epocNano) {
        if (!timers.isEmpty()) {
            for (Timer timer : timers) {
                if (epocNano < ((TimerImpl)timer).scheduledTime) {
                    return timer;
                }
            }
        }
        return null;
    }

    public class TimerImpl implements Timer {
        final TimerTask timerTask;
        long scheduledTime;
        long registeredTime;

        TimerImpl(TimerTask timerTask) {
            this.timerTask = timerTask;
            scheduledTime = -1;
            registeredTime = -1;
        }

        @Override
        public void scheduleAtEpochNano(long timeDelay) {
            scheduledTime = timeDelay;
            timers.add(this);
        }
    }
}


