package com.gaurav.timer;

public interface TimerTask {
    public void onTimer(Timer timer/*, long scheduledTimer, long registeredTime*/);
}
