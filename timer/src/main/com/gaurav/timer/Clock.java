package com.gaurav.timer;

public interface Clock {
    long getEpocNano();
}
