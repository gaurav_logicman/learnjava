package com.gaurav.timer;

public interface Timer {

    public void scheduleAtEpochNano(long timeDelay);
}
