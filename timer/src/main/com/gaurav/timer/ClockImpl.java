package com.gaurav.timer;

public class ClockImpl implements Clock{
    @Override
    public long getEpocNano() {
        return System.currentTimeMillis() * 1000_000;
    }
}
