package lambda;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;

public class SortList8 {
    public static void main(String[] args){
        List<Person> people = Arrays.asList(
                new Person("Shah", "Gaurav", 35),
                new Person("Mehta", "Smruti", 32),
                new Person("Shah", "Aarav", 4),
                new Person("Mehta", "Pramod", 63),
                new Person("Mehta", "Jyoti", 59)
        );

        Collections.sort(people, (p1, p2) -> p1.getLastName().compareTo(p2.getLastName()));

        printConditional(people, (p) -> true);
        System.out.println("==================================");

        printConditional(people, (p) -> p.getAge() > 35);
        System.out.println("==================================");
        printConditionalPredicate(people, (p) -> p.getAge() < 35);

    }

    private static void printConditionalPredicate(List<Person> people, Predicate<Person> predicate){
        for (Person p : people){
            if(predicate.test(p)){
                System.out.println(p);
            }
        }
    }

    private static void printConditional(List<Person> people, Condition condition){
        for(Person p : people){
            if(condition.ifPassed(p)){
                System.out.println(p);
            }
        }
    }

    private static void printAll(List<Person> people) {
        for (Person p : people){
            System.out.println(p);
        }
    }

    interface Condition {
        public boolean ifPassed(Person p);
    }
}
