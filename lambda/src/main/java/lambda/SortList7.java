package lambda;

import java.lang.reflect.Array;
import java.util.*;

public class SortList7 {
    public static void main(String[] args){
        List<Person> people = Arrays.asList(
                new Person("Shah", "Gaurav", 35),
                new Person("Mehta", "Smruti", 32),
                new Person("Shah", "Aarav", 4),
                new Person("Mehta", "Pramod", 63),
                new Person("Mehta", "Jyoti", 59)
        );

        Collections.sort(people, new Comparator<Person>() {
            public int compare(Person o1, Person o2) {
                return o1.getLastName().compareTo(o2.getLastName());
            }
        });

        printAll(people);
        System.out.println("==================================");
        printAboveAge(people, new Condition() {

            @Override
            public boolean ifPassed(Person p) {
                if (p.getAge() > 35){
                    return true;
                }
                return false;
            }
        });
        System.out.println("==================================");
    }

    private static void printAboveAge(List<Person> people, Condition condition) {
        for (Person p : people){
            if (condition.ifPassed(p)){
                System.out.println(p);
            }
        }
    }

    private static void printAll(List<Person> people) {
        for (Person p : people){
            System.out.println(p);
        }
    }

    interface Condition {
        public boolean ifPassed(Person p);
    }
}
