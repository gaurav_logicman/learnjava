package lambda;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class ExceptionHandlingExample {
    public static void main(String[] args){
        int [] someNumbers = {1, 2, 3,4,5};
        int key = 2;

        //process(someNumbers, key, (a, b) -> System.out.println(a + b));

        key = 0;
        process(someNumbers, key, exceptionLambda((a, b)-> System.out.println(a/b)));
    }

    private static BiConsumer<Integer, Integer> exceptionLambda(BiConsumer<Integer, Integer> consumer){
        return (a,b) -> {
            try {
                consumer.accept(a, b);
            }catch (ArithmeticException e){
                System.out.println("Exception Caught!!");
            }
        };
    }

    private static void process(int[] someNumbers, int key, BiConsumer<Integer, Integer> consumer) {
        for (int i : someNumbers){
            consumer.accept(i , key);
        }
    }
}
