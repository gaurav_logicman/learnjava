package lambda;

public class ClosureExample {
    public static void main(String [] args){

        int a = 10;
        int b = 20; // you can not change value of b once it is used in lambada.
        doProcess(a, (i) -> System.out.println(i * b) );

    }
    public static void doProcess(int i, Process p ){
        p.process(i);
    }
}

interface Process {
    void process(int i);
}
