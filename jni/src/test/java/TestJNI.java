import java.nio.ByteBuffer;

public class TestJNI {

    public native void sendStudent(ByteBuffer byteBuffer);

    public static void main(String[] argv){
        Student student = new Student();
//        student.grades[2].set(2.4f);
        student.name.set("Gaurav Shah");

        ByteBuffer byteBuffer = student.getByteBuffer();

        TestJNI testJNI = new TestJNI();
        testJNI.sendStudent(byteBuffer);

    }
}
