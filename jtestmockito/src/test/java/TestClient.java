import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestClient {

    @Mock
    Product product;

    @Mock
    Orders orders;

    @InjectMocks
    Client client = new Client();

    @Test
    public void TestClient_Construction(){
        //When
        when(product.getName()).thenReturn("Toy");
        when(orders.getId()).thenReturn(10);

        assertEquals(client.getOrdersID(), 20);
    }

}
