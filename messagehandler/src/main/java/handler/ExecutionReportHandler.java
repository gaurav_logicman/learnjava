package handler;


import messages.ExecutionReport;
import messages.MyMessage;

public class ExecutionReportHandler implements MessageHandler {
    public boolean processMessage(MyMessage message) {
        System.out.println("Processing Response " + message.name());
        return true;
    }

}
