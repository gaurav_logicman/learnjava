package handler;

import messages.MyMessage;

public interface MessageHandler {
    public boolean processMessage(MyMessage message);
}
