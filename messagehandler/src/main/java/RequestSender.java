import messages.NewOrderRequest;

public class RequestSender {
    private Responder responder;
    public RequestSender(Responder responder){
        this.responder = responder;
    }
    public boolean sendRequest(){
        NewOrderRequest newThreadAction = new NewOrderRequest();
        this.responder.processRequest(newThreadAction, responder );
        return false;
    }
}
