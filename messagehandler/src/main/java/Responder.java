import handler.MessageHandler;
import messages.ExecutionReport;
import messages.MyMessage;
import messages.NewOrderRequest;

public class Responder {


    public void processRequest(MyMessage message, MessageHandler handler) {
        System.out.println("Processing " + message.name());
        handler.processMessage(getResponse(message));
    }

    private MyMessage getResponse(MyMessage message){
        if(message.name() == "NewOrderSingle"){
            return new ExecutionReport();
        }
        return null;
    }
}
